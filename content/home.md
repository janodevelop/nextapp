---
title: Awesome kitties
date: 2019-03-17T19:31:20.591Z
cats:
  - description: 'Maru is a Scottish Fold from Japan, and he loves boxes.'
    name: Maru (まる)
  - description: Lil Bub is an American celebrity cat known for her unique appearance.
    name: Lil Bub
  - description: Grumpy cat is an American celebrity cat known for her grumpy appearance.
    name: Grumpy cat (Tardar Sauce)
  - description: >-
      The Pallas's cat (Otocolobus manul), also called manul, is a small wild
      cat with a broad, but fragmented distribution in the grasslands and
      montane steppes of Central Asia. It is negatively affected by habitat
      degradation, prey base decline and hunting, and has therefore been
      classified as Near Threatened on the IUCN Red List since 2002.[2]
    name: Manul
  - description: Kecsek labu macska merrre jarsz?
    name: 'Kecske labu '
---
Welcome to my awesome page about cats of the internet.

This page is built with NextJS, and content is managed in Netlify CMSklkmlkm
